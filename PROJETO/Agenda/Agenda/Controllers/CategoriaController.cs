﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Agenda.Models;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace Agenda.Controllers
{
    public class CategoriaController : Controller
    {

        public ActionResult Index()
        {
            using (AgendaContext agendaContext = new AgendaContext())
            {
                List<Categoria> categorias = agendaContext.categorias.ToList();
                return View( categorias );
            }
        }

        public ActionResult Incluir()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Incluir(Categoria categoria)
        {
            // Utilizando procedure
            string connectionString = ConfigurationManager.ConnectionStrings["AgendaContext"].ConnectionString;

            try
            {
                using (SqlConnection con = new SqlConnection( connectionString ))
                {
                    SqlCommand cmd = new SqlCommand( "sp_InserirCategoria", con );
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlParameter sqlParameterNome = new SqlParameter() { SqlValue = categoria.CategoriaNome, ParameterName = "@CategoriaNome" };

                    cmd.Parameters.Add( sqlParameterNome );

                    con.Open();
                    cmd.ExecuteNonQuery();
                }
                return RedirectToAction( "Index" );

            }
            catch
            {
                ViewBag.Error = "Impossivel incluir!";
                return View();
            }
        }

        [HttpPost]
        [ActionName( "Excluir" )]
        public string Excluir(int id)
        {
            using (AgendaContext agendaContext = new AgendaContext())
            {
                Categoria categoria = agendaContext.categorias.Single(cat => cat.CategoriaId == id);
                agendaContext.categorias.Remove( categoria );
                try
                {
                    agendaContext.SaveChanges();
                    return "Excluida com sucesso!";
                }
                catch
                {
                    return "Erro ao tentar excluir!";
                }
            }

        }
        
        [HttpPost]
        [ActionName("_ListarCategoria")]
        public ActionResult _ListarCategoria()
        {
            using (AgendaContext agendaContext = new AgendaContext())
            {
                List<Categoria> categorias = agendaContext.categorias.ToList();
                return View( categorias );
            }
        }
    }
}