﻿using Agenda.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Agenda.Controllers
{
    public class EventoController : Controller
    {
        private static DateTime dataIni = DateTime.Today;
        private static DateTime dataFim = DateTime.Today.AddDays( 1 );

        public ActionResult Index()
        {
            dataIni = DateTime.Today;
            dataFim = DateTime.Today.AddDays( 1 );
            using (AgendaContext agendaContext = new AgendaContext())
            {
                List<Evento> eventos = agendaContext.eventos.ToList();
                return View( eventos );
            }
        }

        public ActionResult Incluir()
        {
            CarregarCategorias();
            return View();
        }

        [HttpPost]
        /**Verificar*/
        public ActionResult Incluir(Evento evento)
        {
            CarregarCategorias();
            // Utilizando procedure
            string connectionString = ConfigurationManager.ConnectionStrings["AgendaContext"].ConnectionString;

            try
            {
                using (SqlConnection con = new SqlConnection( connectionString ))
                {
                    SqlCommand cmd = new SqlCommand( "sp_InserirEvento", con );
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlParameter sqlParameterNome = new SqlParameter() { SqlValue = evento.EventoNome, ParameterName = "@EventoNome" };
                    SqlParameter sqlParameterDescricao = new SqlParameter() { SqlValue = evento.EventoDescricao, ParameterName = "@Eventodescricao" };
                    SqlParameter sqlParameterDataIni = new SqlParameter() { SqlValue = evento.EventoDataInicio, ParameterName = "@EventoDataInicio" };
                    SqlParameter sqlParameterDataFim = new SqlParameter() { SqlValue = evento.EventoDataFim, ParameterName = "@EventoDataFim" };
                    SqlParameter sqlParameterStatus = new SqlParameter() { SqlValue = evento.EventoStatus, ParameterName = "@EventoStatus" };
                    SqlParameter sqlParameterCategoria = new SqlParameter() { SqlValue = evento.CategoriaId, ParameterName = "@CategoriaId" };

                    cmd.Parameters.Add( sqlParameterNome );
                    cmd.Parameters.Add( sqlParameterDescricao );
                    cmd.Parameters.Add( sqlParameterDataIni );
                    cmd.Parameters.Add( sqlParameterDataFim );
                    cmd.Parameters.Add( sqlParameterStatus );
                    cmd.Parameters.Add( sqlParameterCategoria );

                    con.Open();
                    cmd.ExecuteNonQuery();
                }
                return RedirectToAction( "Index" );

            }
            catch (Exception e)
            {
                ViewBag.Error = e.Message;
                return View();
            }
        }

        [HttpPost]
        [ActionName( "Excluir" )]
        public string Excluir(int id)
        {
            using (AgendaContext agendaContext = new AgendaContext())
            {
                Evento evento = agendaContext.eventos.Single( eve => eve.EventoId == id );
                agendaContext.eventos.Remove( evento );
                try
                {
                    agendaContext.SaveChanges();
                    return "Sucesso";
                }
                catch
                {
                    return "Impossivel excluir!";
                }
            }
        }

        [HttpPost]
        [ActionName( "_ListarEventos" )]
        public ActionResult _ListarEventos()
        {
            return View( GetEventos() );
        }

        private List<Evento> GetEventos()
        {
            using (AgendaContext agendaContext = new AgendaContext())
            {
                return agendaContext.eventos.ToList().Where( eve => eve.EventoDataInicio > dataIni && eve.EventoDataFim < dataFim ).ToList();
            }
        }

        private void CarregarCategorias()
        {
            List<Categoria> categorias = new List<Categoria>();
            using (AgendaContext agendaContext = new AgendaContext())
            {
                categorias = agendaContext.categorias.ToList();
                ViewBag.Categorias = categorias;
            }

        }

        [HttpPost]
        [ActionName( "TestarDatas" )]
        public string TestarDatas(string dateTimeInicio, string dateTimeFim)
        {
            try
            {
                DateTime dateTimeI = Convert.ToDateTime( dateTimeInicio );
                DateTime dateTimeF = Convert.ToDateTime( dateTimeFim );
                int res = dateTimeF.CompareTo( dateTimeI );
                if (res == -1)
                {
                    return "Da inicial deve ser menor que data final.";

                }
                dataIni = dateTimeI;
                dataFim = dateTimeF;
                return "";
            }
            catch
            {
                return "Impossivel converter datas";
            }
        }

        [HttpPost]
        [ActionName( "Finalizar" )]
        public string Finalizar(int id)
        {
            using (AgendaContext agendaContext = new AgendaContext())
            {
                Evento evento = agendaContext.eventos.Single(eve => eve.EventoId == id);
                evento.EventoStatus = "F";
                try
                {
                    agendaContext.SaveChanges();
                    return "Finalizado com sucesso";
                }
                catch
                {
                    return "Não foi possivel finalizar!";
                }
            }
            //string connectionString = ConfigurationManager.ConnectionStrings["AgendaContext"].ConnectionString;
            //try
            //{
            //    using (SqlConnection con = new SqlConnection( connectionString ))
            //    {
            //        SqlCommand cmd = new SqlCommand( "sp_FecharEvento", con );
            //        cmd.CommandType = CommandType.StoredProcedure;
            //        cmd.Parameters.Add( new SqlParameter() { ParameterName = "@EventoId", SqlValue = id } );
            //        con.Open();
            //        cmd.ExecuteNonQuery();
            //        return "Finalizado com sucesso";
            //    }

            //}
            //catch
            //{
            //    return "Não foi possivel finalizar!";
            //}
        }
    }
}