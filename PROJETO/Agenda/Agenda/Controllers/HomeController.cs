﻿using Agenda.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Agenda.Controllers
{
    public class HomeController : Controller
    {

        public ActionResult Index()
        {
            // Utilizando procedure
            string connectionString = ConfigurationManager.ConnectionStrings["AgendaContext"].ConnectionString;

            List<Evento> eventos = new List<Evento>();
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand( "sp_EventosAbertos", con );
                cmd.CommandType = CommandType.StoredProcedure;

                con.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    Evento evento = new Evento();
                    evento.EventoId = Convert.ToInt32(rdr["evento_id"]);
                    evento.EventoNome = rdr["evento_nome"].ToString();
                    evento.EventoDescricao = rdr["evento_descricao"].ToString();
                    evento.EventoDataInicio = Convert.ToDateTime(rdr["evento_data_inicio"]);
                    evento.EventoDataFim = Convert.ToDateTime( rdr["evento_data_fim"] );
                    evento.EventoStatus = rdr["evento_status"].ToString();
                    eventos.Add(evento);
                }

                return View(eventos);
            }
        }
    }
}