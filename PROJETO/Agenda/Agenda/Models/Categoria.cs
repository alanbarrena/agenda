﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Agenda.Models
{
    // Definindo tabela
    [Table("tbl_Categoria")]
    public class Categoria
    {
        [Column("categoria_id")]
        public int CategoriaId { get; set; }

        [Column( "categoria_nome" )]
        public string CategoriaNome { get; set; }
    }
}