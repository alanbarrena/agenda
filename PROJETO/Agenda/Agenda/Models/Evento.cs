﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Agenda.Models
{
    // Definindo tabela
    [Table( "tbl_Evento" )]
    public class Evento
    {

        [Column("evento_id")]
        public int EventoId { get; set; }

        [Column( "evento_nome" )]
        public string EventoNome { get; set; }

        [Column( "evento_descricao" )]
        public string EventoDescricao { get; set; }

        [Column( "evento_data_inicio" )]
        [DataType( DataType.DateTime )]
        [DisplayFormat( DataFormatString = "{0:dd/MM/yyyy HH:mm:ss}", ApplyFormatInEditMode = true )]
        public DateTime EventoDataInicio { get; set; }

        [Column( "evento_data_fim" )]
        [DataType( DataType.DateTime )]
        [DisplayFormat( DataFormatString = "{0:dd/MM/yyyy HH:mm:ss}", ApplyFormatInEditMode = true )]
        public DateTime EventoDataFim { get; set; }

        [Column( "evento_status" )]
        public string EventoStatus { get; set; }

        [Column( "categoria_id" )]
        public int CategoriaId { get; set; }

        public virtual string CategoriaNome
        {
            get
            {
                using (AgendaContext agendaContext = new AgendaContext())
                {
                    Categoria categoria = agendaContext.categorias.Single( cat => cat.CategoriaId == CategoriaId );
                    return categoria.CategoriaNome;
                }
            }
        }

        public virtual string StatusNome
        {
            get
            {
                if (EventoStatus.Equals("A"))
                {
                    return  "Aberto";
                }
                else if (EventoStatus.Equals( "F" ))
                {
                    return "Fechado";
                }
                else
                {
                    return "Não informado";
                }
            }
        }
    }
}