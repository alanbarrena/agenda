﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Agenda.Models
{
    public class AgendaContext : DbContext
    {
        public AgendaContext() : base( "AgendaContext" )
        {
            Database.SetInitializer<AgendaContext>( null );
        }

        public DbSet<Evento> eventos { get; set; }
        public DbSet<Categoria> categorias { get; set; }

    }
}