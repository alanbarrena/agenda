CREATE DATABASE [Agenda]
GO

USE [Agenda]
GO
/****** Object:  Table [dbo].[tbl_Categoria]    Script Date: 21/11/2017 01:34:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Categoria](
	[categoria_id] [int] IDENTITY(1,1) NOT NULL,
	[categoria_nome] [varchar](30) NOT NULL,
 CONSTRAINT [PK_tbl_Categoria] PRIMARY KEY CLUSTERED 
(
	[categoria_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Evento]    Script Date: 21/11/2017 01:34:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Evento](
	[evento_id] [int] IDENTITY(1,1) NOT NULL,
	[evento_nome] [varchar](50) NOT NULL,
	[evento_descricao] [varchar](200) NULL,
	[evento_data_inicio] [datetime] NOT NULL,
	[evento_data_fim] [datetime] NULL,
	[evento_status] [char](1) NOT NULL,
	[categoria_id] [int] NOT NULL,
 CONSTRAINT [PK_tbl_Evento] PRIMARY KEY CLUSTERED 
(
	[evento_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_Evento]  WITH CHECK ADD  CONSTRAINT [FK_tbl_Evento_tbl_Categoria] FOREIGN KEY([categoria_id])
REFERENCES [dbo].[tbl_Categoria] ([categoria_id])
GO
ALTER TABLE [dbo].[tbl_Evento] CHECK CONSTRAINT [FK_tbl_Evento_tbl_Categoria]
GO
/****** Object:  StoredProcedure [dbo].[sp_EventosAbertos]    Script Date: 21/11/2017 01:34:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_EventosAbertos]
AS
BEGIN
	SELECT [evento_id]
		  ,[evento_nome]
		  ,[evento_descricao]
		  ,[evento_data_inicio]
		  ,[evento_data_fim]
		  ,[evento_status]
		  ,[categoria_id]
	  FROM [tbl_Evento] T
	 WHERE T.evento_status = 'A'
	   AND T.evento_data_inicio > GETDATE()
	ORDER BY T.evento_data_inicio ASC
END
GO
/****** Object:  StoredProcedure [dbo].[sp_FecharEvento]    Script Date: 21/11/2017 01:34:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_FecharEvento] 
	@EventoId int
AS
BEGIN
	UPDATE [dbo].[tbl_Evento]
	   SET [evento_status] = 'F'
	 WHERE evento_id = @EventoId
END
GO
/****** Object:  StoredProcedure [dbo].[sp_InserirCategoria]    Script Date: 21/11/2017 01:34:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_InserirCategoria] 
	@CategoriaNome varchar(30)
AS
BEGIN
	INSERT INTO [dbo].[tbl_Categoria]
			   ([categoria_nome])
		 VALUES
			   (UPPER(@CategoriaNome))
END
GO
/****** Object:  StoredProcedure [dbo].[sp_InserirEvento]    Script Date: 21/11/2017 01:34:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_InserirEvento] 
	@EventoNome varchar(50),
	@Eventodescricao varchar(200),
	@EventoDataInicio datetime = GETDATE,
	@EventoDataFim datetime,
	@EventoStatus char(1) = 'A',
	@CategoriaId int
AS
BEGIN
	IF(@EventoDataFim >= @EventoDataInicio)
	BEGIN
		INSERT INTO [dbo].[tbl_Evento]
			   ([evento_nome]
			   ,[evento_descricao]
			   ,[evento_data_inicio]
			   ,[evento_data_fim]
			   ,[evento_status]
			   ,[categoria_id])
		 VALUES
			   (
		UPPER(@EventoNome),
		UPPER(@Eventodescricao),
		@EventoDataInicio,
		@EventoDataFim,
		@EventoStatus,
		@CategoriaId)
	END
	ELSE
	BEGIN
		RAISERROR('Data fim deve ser maior ou igual a data inicio!', 16, 1);
	END
END
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'A - Aberto, F - Fechado' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbl_Evento', @level2type=N'COLUMN',@level2name=N'evento_status'
GO
